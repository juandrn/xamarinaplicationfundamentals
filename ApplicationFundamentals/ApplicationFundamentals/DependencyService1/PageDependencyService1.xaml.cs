﻿using ApplicationFundamentals.DependencyService1.ImplementingTextToSpeech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.DependencyService1
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PageDependencyService1 : ContentPage
	{
		public PageDependencyService1 ()
		{
			InitializeComponent ();
		}

        private void Button_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ITextToSpeech>().Speak("Hellow from Xamarin Forms");
        }
    }
}