﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationFundamentals.LocalDatabases
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
