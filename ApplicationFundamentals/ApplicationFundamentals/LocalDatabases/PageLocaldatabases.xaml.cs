﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.LocalDatabases
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageLocaldatabases : ContentPage
    {
        private ObservableCollection<TodoItem> items { get; set; }

        public PageLocaldatabases()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            this.items = new ObservableCollection<TodoItem>(await App.Database.GetItemsAsync());

            this.list.ItemsSource = items;
        }

        private void Insertar_Clicked(object sender, EventArgs e)
        {
            var todoItem = new TodoItem()
            {
                Name = this.input.Text,
                Notes = this.inputNotes.Text,
            };
            if (!string.IsNullOrEmpty(todoItem.Notes) &&
                !string.IsNullOrEmpty(todoItem.Name))
            {
                App.Database.SaveItemAsync(todoItem);
                this.items.Add(todoItem);
            }
        }

        private void Eliminar_Clicked(object sender, EventArgs e)
        {
            var todoItem = list.SelectedItem as TodoItem;

            App.Database.DeleteItemAsync(todoItem);
            this.items.Remove(todoItem);
        }
    }
}