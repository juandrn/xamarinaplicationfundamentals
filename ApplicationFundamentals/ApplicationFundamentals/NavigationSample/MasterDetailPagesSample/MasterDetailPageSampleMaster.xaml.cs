﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.NavigationSample.MasterDetailPagesSample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterDetailPageSampleMaster : ContentPage
    {
        public ListView ListView;

        public MasterDetailPageSampleMaster()
        {
            InitializeComponent();

            BindingContext = new MasterDetailPageSampleMasterViewModel();
            ListView = MenuItemsListView;
        }

        class MasterDetailPageSampleMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<MasterDetailPageSampleMenuItem> MenuItems { get; set; }
            
            public MasterDetailPageSampleMasterViewModel()
            {
                MenuItems = new ObservableCollection<MasterDetailPageSampleMenuItem>(new[]
                {
                    new MasterDetailPageSampleMenuItem { Id = 0, Title = "Page 1" },
                    new MasterDetailPageSampleMenuItem { Id = 1, Title = "Page 2" },
                    new MasterDetailPageSampleMenuItem { Id = 2, Title = "Page 3" },
                    new MasterDetailPageSampleMenuItem { Id = 3, Title = "Page 4" },
                    new MasterDetailPageSampleMenuItem { Id = 4, Title = "Page 5" },
                });
            }
            
            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}