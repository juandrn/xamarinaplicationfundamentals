﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationFundamentals.NavigationSample.MasterDetailPagesSample
{

    public class MasterDetailPageSampleMenuItem
    {
        public MasterDetailPageSampleMenuItem()
        {
            TargetType = typeof(MasterDetailPageSampleDetail);
        }
        public int Id { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }
    }
}