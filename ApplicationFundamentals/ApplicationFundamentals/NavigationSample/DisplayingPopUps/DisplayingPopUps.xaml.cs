﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.NavigationSample.DisplayingPopUps
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DisplayingPopUps : ContentPage
	{
		public DisplayingPopUps ()
		{
			InitializeComponent ();
		}
	}
}