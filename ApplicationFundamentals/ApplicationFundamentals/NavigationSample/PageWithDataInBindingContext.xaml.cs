﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.NavigationSample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageWithDataInBindingContext : ContentPage
    {
        public static int Counter { get; set; }
        public int Number { get; set; }
        public PageWithDataInBindingContext()
        {
            InitializeComponent();
        }

        private async void BackButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private async void AutocallmeButton_Clicked(object sender, EventArgs e)
        {
            await this.Navigation.PushAsync(new PageWithDataInBindingContext() { Number = Counter++ });
        }
    }
}