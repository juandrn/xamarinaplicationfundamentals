﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.NavigationSample.ModalPagesSample
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ModalPageSample : ContentPage
	{
		public ModalPageSample ()
		{
			InitializeComponent ();
		}

        private void Cerrar_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }
    }
}