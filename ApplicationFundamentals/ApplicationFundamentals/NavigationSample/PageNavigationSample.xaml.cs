﻿using ApplicationFundamentals.LocalDatabases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.NavigationSample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageNavigationSample : ContentPage
    {
        public PageNavigationSample()
        {
            InitializeComponent();
        }

        private async void PassDataThroughBinding_Clicked(object sender, EventArgs e)
        {
            var asdf = new PageWithDataInBindingContext();
            asdf.BindingContext = new TodoItem { Name = "By Binding Context Name", Notes = "By Binding Context Notes" };
            await Navigation.PushAsync(asdf);
        }

        private void Tabbed_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new TabbedPageSample());
        }

        private void Carousel_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CarouselPages.CarouselPageSample());
        }

        private void CarouselWithItemTemplate_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CarouselPages.CarouselPageWithTemplate());
        }

        private void MasterDetail_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MasterDetailPagesSample.MasterDetailPageSample());
        }

        private void Modal_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ModalPagesSample.ModalPageSample());
        }

        private async void PopUp_Clicked(object sender, EventArgs e)
        {
            var action = await DisplayActionSheet("ActionSheet: Send to?", "Cancel", null, "Email", "Twitter", "Facebook");
            var answer = await DisplayAlert("Question?", "Has seleccionado " + action + "?", "Yes", "No");
            await DisplayAlert("Info", "Respondiste que " + answer + " seleccionaste " + action, "Cerrar");
        }
    }
}