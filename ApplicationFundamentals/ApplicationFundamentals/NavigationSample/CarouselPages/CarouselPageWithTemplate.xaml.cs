﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.NavigationSample.CarouselPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CarouselPageWithTemplate : CarouselPage
    {
        public CarouselPageWithTemplate()
        {
            InitializeComponent();

            this.ItemsSource = new List<dynamic>
            {
                new { Name= "Red", Color = Color.Red },
                new { Name= "Green", Color = Color.Green},
                new { Name= "Blue", Color = Color.Blue}
            };
        }
    }
}