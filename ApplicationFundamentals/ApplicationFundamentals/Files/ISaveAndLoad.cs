﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationFundamentals.Files
{
    public interface ISaveAndLoad
    {
        void SaveText(string filename, string text);
        string LoadText(string filename);
    }
}
