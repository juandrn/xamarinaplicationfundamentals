﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.Files
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageFiles : ContentPage
    {
        public PageFiles()
        {
            InitializeComponent();
        }

        private void ReadTextFile_Clicked(object sender, EventArgs e)
        {
            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(PageFiles)).Assembly;
            Stream stream = assembly.GetManifestResourceStream("ApplicationFundamentals.Files.TextFile.txt");
            string text = "";
            using (var reader = new System.IO.StreamReader(stream))
            {
                text = reader.ReadToEnd();
            }
            this.data.Text = text;
        }

        private void saveData_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<ISaveAndLoad>().SaveText("temp.txt", input.Text);
        }

        private void readData_Clicked(object sender, EventArgs e)
        {
            this.data.Text = DependencyService.Get<ISaveAndLoad>().LoadText("temp.txt");
        }
    }
}