﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.MessagingCenterSample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageMessagingCenter : ContentPage
    {
        public PageMessagingCenter()
        {
            InitializeComponent();

            MessagingCenter.Subscribe<PageMessagingCenter, string>(this, "Message", (sender, arg) =>
            {
                this.label.Text = arg;

            });
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            MessagingCenter.Send(this, "Message", DateTime.Now.ToString());

        }
    }
}