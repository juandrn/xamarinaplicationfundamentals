﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace ApplicationFundamentals.Accessibility
{
    /// <summary>
    /// Ver ejemplo en clase App
    /// </summary>
    public class PagePID : ContentPage
    {
        public PagePID()
        {
            Content = new StackLayout
            {
                Children = {
                    new Label { Text =$"PID:      {SessionConstants.PID}." },
                    new Label { Text =$"LAST_PID: {SessionConstants.LAST_PID}." },
                }
            };
        }
    }
}