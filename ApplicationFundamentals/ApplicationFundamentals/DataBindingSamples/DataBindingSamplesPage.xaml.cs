﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.DataBindingSamples
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DataBindingSamplesPage : ContentPage
    {
        public DataBindingSamplesPage()
        {
            InitializeComponent();
        }

        private void BasicBindings_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new DataBindingSamples.BasicBindingsSample.BasicBindingsSamplePage());
        }

        private void BasicBindingsByXaml_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new DataBindingSamples.BasicBindingsSample.BasicBindingsSamplePage());
        }

        private void BindingMode_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new DataBindingSamples.BindingModeSample.BindingModeSamplePage());
        }

        private void StringFormat_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new DataBindingSamples.StringFormatting.StringFormattingSamplePage());
        }

        private void BindingPath_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new DataBindingSamples.BindingPathSample.BindingPathSamplePage2());
        }

        private void BindingValueConvertes_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new BindingValueConvertersSample.BindingValueConvertersSamplePage());
        }

        private void TheCommandInterface_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new DataBindingSamples.TheCommandInterfaceSample.TheCommandInterfaceSamplePage());
        }
    }
}