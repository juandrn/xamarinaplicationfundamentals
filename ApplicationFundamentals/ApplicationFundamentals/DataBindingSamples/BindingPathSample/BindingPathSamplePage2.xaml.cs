﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.DataBindingSamples.BindingPathSample
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BindingPathSamplePage2 : ContentPage
	{
		public BindingPathSamplePage2 ()
		{
			InitializeComponent ();
		}
	}
}