﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.DataBindingSamples.BindingValueConvertersSample
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BindingValueConvertersSamplePage : ContentPage
	{
		public BindingValueConvertersSamplePage ()
		{
			InitializeComponent ();
		}
	}
}