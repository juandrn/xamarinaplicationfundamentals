﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.DataBindingSamples.BasicBindingsSample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BasicBindingsSamplePage : ContentPage
    {
        public BasicBindingsSamplePage()
        {
            InitializeComponent();

            label.SetBinding(RotationProperty, new Binding("Value", source: slider));
        }
    }
}