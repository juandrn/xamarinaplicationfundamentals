﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.GesturesSample.TapSample
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TapSamplePage : ContentPage
	{
        private int tapCount;

        public TapSamplePage ()
		{
			InitializeComponent ();
		}

        void OnTapGestureRecognizerTapped(object sender, EventArgs args)
        {
            tapCount++;
            var imageSender = (Image)sender;
            // watch the monkey go from color to black&white!
            if (tapCount % 2 == 0)
            {
                imageSender.Source = "tapped.jpg";
            }
            else
            {
                imageSender.Source = "tapped_bw.jpg";
            }
        }
    }
}