﻿using ApplicationFundamentals.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ApplicationFundamentals.GesturesSample.TapSample
{
    public class TapViewModel : NotifyPropertyChangedBase
    {
        int taps = 0;
        private readonly ICommand tapCommand;
        public TapViewModel()
        {
            // configure the TapCommand with a method
            tapCommand = new Command(OnTapped);
        }
        public ICommand TapCommand
        {
            get { return tapCommand; }
        }

        public int Taps { get => taps; set => base.SetProperty(ref taps, value); }

        void OnTapped(object s)
        {
            Taps++;
            Debug.WriteLine("parameter: " + s);
        }
        //region INotifyPropertyChanged code omitted
    }
}
