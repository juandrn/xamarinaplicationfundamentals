﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.GesturesSample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GestureSamplePage : ContentPage
    {
        public GestureSamplePage()
        {
            InitializeComponent();
        }

        private void GestureTap_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new TapSample.TapSamplePage());
        }

        private void GesturePinch_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PinchSample.PinchSamplePage());
        }

        private void GesturePan_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PanSample.PanSamplePage());
        }
    }
}