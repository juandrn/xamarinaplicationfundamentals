using ApplicationFundamentals.DataBindingSamples.BindingModeSample;
using ApplicationFundamentals.LocalDatabases;
using System;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ApplicationFundamentals
{
    public partial class App : Application
    {
        public static double ScreenWidth;
        public static double ScreenHeight;

        static TodoItemDatabase database;
        public SampleSettingsViewModel Settings { private set; get; }
        public static TodoItemDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new TodoItemDatabase(DependencyService.Get<IFileHelper>().GetLocalFilePath("TodoSQLite1.db3"));
                }
                return database;
            }
        }

        public App()
        {
            InitializeComponent();

            Settings = new SampleSettingsViewModel(Current.Properties);

            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            Debug.WriteLine("OnStart");
            if (Current.Properties.ContainsKey("PID"))
            {
                SessionConstants.LAST_PID = ":OnStart:" + (Current.Properties["PID"].ToString());
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            Debug.WriteLine("OnSleep");
            Current.Properties["PID"] = ":OnSleep:" + SessionConstants.PID;

            Settings.SaveState(Current.Properties);
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            Debug.WriteLine("OnResume");
            if (Current.Properties.ContainsKey("PID"))
            {
                SessionConstants.LAST_PID = ":OnResume:" + (Current.Properties["PID"].ToString());
            }
        }

    }
}
