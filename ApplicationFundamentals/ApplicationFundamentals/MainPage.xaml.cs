﻿using ApplicationFundamentals.Accessibility;
using ApplicationFundamentals.Behaviours;
using ApplicationFundamentals.DependencyService1;
using ApplicationFundamentals.Files;
using ApplicationFundamentals.LocalDatabases;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ApplicationFundamentals
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            SessionConstants.PID = "MAIN PAGE" + Process.GetCurrentProcess().Id.ToString();
            this.platform.Text = Environment.OSVersion.Platform.ToString() + "-" + Environment.OSVersion.VersionString;

            NavigateCommand = new Command<Type>(
            async (Type pageType) =>
            {
                Page page = (Page)Activator.CreateInstance(pageType);
                await Navigation.PushAsync(page);
            });
            this.BindingContext = this;
        }
        public ICommand NavigateCommand { private set; get; }

        private void AppClass_Click(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PagePID());
        }

        private void Behaviours_Click(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PageBehaviours());
        }

        private void DependencyService_Click(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PageDependencyService1());
        }

        private void File_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PageFiles());
        }

        private void LocalDatabases_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PageLocaldatabases());
        }

        private void MessagingCenterButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MessagingCenterSample.PageMessagingCenter());
        }

        private void Navigation_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new NavigationSample.PageNavigationSample());
        }

        private void CustomRenderer_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CustomRenderers.CutomRenderersSamplePage());
        }

        private void BindingBasic_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new DataBindingSamples.DataBindingSamplesPage());
        }
    }
}
