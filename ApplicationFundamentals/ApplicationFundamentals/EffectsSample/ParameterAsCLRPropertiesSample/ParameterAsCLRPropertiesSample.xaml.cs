﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.EffectsSample.ParameterAsCLRPropertiesSample
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ParameterAsCLRPropertiesSample : ContentPage
	{
		public ParameterAsCLRPropertiesSample ()
		{
			InitializeComponent ();

            Color color = Color.Default;
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    color = Color.Black;
                    break;
                case Device.Android:
                    color = Color.White;
                    break;
                case Device.UWP:
                    color = Color.Red;
                    break;
            }

            labelEffectByCSharp.Effects.Add(new ShadowEffect
            {
                Radius = 5,
                Color = color,
                DistanceX = 5,
                DistanceY = 5
            });
        }
	}
}