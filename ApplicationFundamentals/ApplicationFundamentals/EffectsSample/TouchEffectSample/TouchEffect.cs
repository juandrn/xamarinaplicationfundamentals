﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ApplicationFundamentals.EffectsSample.TouchEffectSample
{
    public class TouchEffect : RoutingEffect
    {
        public event TouchActionEventHandler TouchAction;

        public TouchEffect() : base("XamarinDocs.TouchEffect")
        {
        }
        public bool Capture { set; get; }

        public void OnTouchAction(Element element, TouchActionEventArgs args)
        {
            TouchAction?.Invoke(element, args);
        }
    }
}
