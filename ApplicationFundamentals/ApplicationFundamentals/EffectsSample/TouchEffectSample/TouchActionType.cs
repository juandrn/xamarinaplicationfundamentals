﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationFundamentals.EffectsSample.TouchEffectSample
{
    public enum TouchActionType
    {
        Entered,
        Pressed,
        Moved,
        Released,
        Exited,
        Cancelled
    }
}
