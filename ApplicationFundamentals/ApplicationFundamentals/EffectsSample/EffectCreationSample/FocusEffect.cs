﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ApplicationFundamentals.EffectsSample.EffectCreationSample
{
    public class FocusEffect : RoutingEffect
    {
        public FocusEffect() : base("MyCompany.FocusEffect")
        {
        }
    }
}
