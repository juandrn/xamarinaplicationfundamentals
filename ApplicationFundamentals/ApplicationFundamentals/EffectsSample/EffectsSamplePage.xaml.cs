﻿using ApplicationFundamentals.EffectsSample.EffectCreationSample;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.EffectsSample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EffectsSamplePage : ContentPage
    {
        public EffectsSamplePage()
        {
            InitializeComponent();
        }

        private void EffectCreation_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new EffectCreationSamplePage());
        }

        private void ParameterAsCLRPropertiesSample_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ParameterAsCLRPropertiesSample.ParameterAsCLRPropertiesSample());
        }

        private void PrametersAsAttache3dProperties_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ParametersAsAttachedPropertiesSample.ParametersAsAttachedPropertiesSamplePagew());
        }
    }
}