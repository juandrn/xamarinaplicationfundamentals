﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.EffectsSample.ParametersAsAttachedPropertiesSample
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ParametersAsAttachedPropertiesSamplePagew : ContentPage
    {
        public ParametersAsAttachedPropertiesSamplePagew()
        {
            InitializeComponent();

            this.Appearing += ParametersAsAttachedPropertiesSamplePagew_Appearing;

        }

        private void ParametersAsAttachedPropertiesSamplePagew_Appearing(object sender, EventArgs e)
        {
            Color color = Color.Default;
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    color = Color.Black;
                    break;
                case Device.Android:
                    color = Color.Black;
                    break;
                case Device.UWP:
                    color = Color.Red;
                    break;
            }

            var label = labelShadowEffect;

            ShadowEffect.SetHasShadow(label, true);
            ShadowEffect.SetRadius(label, 5);
            ShadowEffect.SetDistanceX(label, 5);
            ShadowEffect.SetDistanceY(label, 5);
            ShadowEffect.SetColor(label, color);
        }

        private void ApplyEffect_Clicked(object sender, EventArgs e)
        {
            ShadowEffect.SetHasShadow(labelShadowEffect, false);
            ShadowEffect.SetHasShadow(labelShadowEffect0, false);
            ShadowEffect.SetHasShadow(labelShadowEffect, true);
            ShadowEffect.SetHasShadow(labelShadowEffect0, true);

        }
    }
}