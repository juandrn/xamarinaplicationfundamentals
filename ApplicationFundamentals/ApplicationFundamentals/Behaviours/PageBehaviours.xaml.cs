﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.Behaviours
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PageBehaviours : ContentPage
	{
		public PageBehaviours ()
		{
			InitializeComponent ();
		}
	}
}