﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ApplicationFundamentals.CustomRenderers
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CutomRenderersSamplePage : ContentPage
    {
        public CutomRenderersSamplePage()
        {
            InitializeComponent();
        }

        private void CustomContentPage_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CustomRenderers.CustomizingAContentPage.CustomizingAContentPagePage());
        }
    }
}