﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ApplicationFundamentals.DependencyService1.ImplementingTextToSpeech;
using ApplicationFundamentals.iOS;
using AVFoundation;
using Foundation;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(TextToSpeech_IOS))]
namespace ApplicationFundamentals.iOS
{
    public class TextToSpeech_IOS : ITextToSpeech
    {
        public TextToSpeech_IOS() { }

        public void Speak(string text)
        {
            var speechSynthesizer = new AVSpeechSynthesizer();
            var speechUtterance = new AVSpeechUtterance(text)
            {
                Rate = AVSpeechUtterance.MaximumSpeechRate / 4,
                Voice = AVSpeechSynthesisVoice.FromLanguage("en-US"),
                Volume = 0.5f,
                PitchMultiplier = 1.0f
            };

            speechSynthesizer.SpeakUtterance(speechUtterance);
        }
    }
}